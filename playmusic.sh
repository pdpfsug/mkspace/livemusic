#!/bin/sh

## Dichiarazione
#OLD: pkg_list="git jack-tools ant fftw3 qjackctl leiningen alsa-utils" # completo
#OLD: pkg_list"git qjackctl alsa-utils leiningen" # parziale
#OLD: pkg_list="$@" # li passo da riga di comando

pkg_list=$(cat deps.txt)
music=${1:-ninixexe.core}

check() {
    [ -x "$(which $1)" ]
}


## Inizio
echo "\n\n~~Sabato alle 15:00 c'e' il MakerSpace~~\n\n"

## Installo i pacchetti se non sono installati
for pkg_name in $pkg_list; do
    if ! check $pkg_name; then
        echo "Errore: $pkg_name non installato. Installazione in corso..." >&2

        if check "pacman"; then
            sudo pacman -S --no-confirm $pkg_name
        elif check "apt"; then
            sudo apt install -y $pkg_name
        fi

    fi
done

watch amixer -c 0 set PCM 100% > /dev/null 2>&1 & # Massimo volume, perche' no? *:

## Scarico il programma livemusic se non esiste già,
# ossia se non esiste la directory
# e se non esiste la directory .git
if ! [ -d "$FILE" ] && ! [ -d .git ]; then
    git clone https://gitlab.com/aredots/livemusic.git
    cd livemusic
fi

pkill -9 "java" #killo le vecchie istanze di java perche uso <CTRL + C> per terminare il programma

# Esegui lein con la tua musica preferita
lein run -m $music
